<?php 
include 'config_bd.php';

//  Récupération de l'utilisateur et de son pass hashé
$req = $bdd->prepare('SELECT nomUser, prenomUser, emailUser, mdpUser FROM user WHERE emailUser = :email');
$req->execute(array(
    'email' => $_POST['email']));
$resultat = $req->fetch();

// Comparaison du pass envoyé via le formulaire avec la base
$isPasswordCorrect = password_verify($_POST['pass'], $resultat['mdpUser']);

if (!$resultat)
{
    echo 'Identifiant incorrect !';
}
else
{
    if ($isPasswordCorrect) {
        session_start();
        $_SESSION['nom'] = $resultat['nomUser'];
        $_SESSION['prenom'] = $resultat['prenomUser'];
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <title>Page accueil</title>
        </head>
        <body>

            Salut <?php echo $_SESSION['nom'] . ' ' . $_SESSION['prenom']; ?> !<br />

        </body>
        </html>
<?php
    }
    else {
        echo 'Mot de passe incorrect !';
    }
}
?>

