<script type="text/javascript">
	function emailExistant(){
		alert("Email déjà existant !");
	}
</script>

<?php 
include 'config_bd.php';

// Vérification de la validité des informations
if ($_POST['pass'] == $_POST['pass2'] && isset($_POST['nom'])){
	//utilisateur déjà existant
	$req = $bdd->prepare('SELECT emailUser FROM user WHERE emailUser = :email');
	$req->execute(array(
	    'email' => $_POST['email']));
	$resultat = $req->fetch();
	$req->closeCursor();

	if (!$resultat){
		
		// Hachage du mot de passe
		$pass_hache = password_hash($_POST['pass'], PASSWORD_DEFAULT);


		// Insertion
		$req = $bdd->prepare('INSERT INTO user(nomUser, prenomUser, emailUser, mdpUser, estAdministrateur) VALUES(:nom, :prenom, :email, :pass, :isAdmin)');
		$req->execute(array(
		    'nom' => $_POST["nom"],
		    'prenom' => $_POST["prenom"],
		    'email' => $_POST["email"],
		    'pass' => $pass_hache,
		    'isAdmin' => 0 
		));
		$req->closeCursor();
	}else{
		echo "Email déjà existant."
	}
	
}
?>